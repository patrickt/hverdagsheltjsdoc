YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "CategoryService",
        "ChangePassword",
        "ChooseCategory",
        "ChooseEventCategory",
        "CountyService",
        "CountySubscription",
        "EmployeeService",
        "EventCategoryService",
        "Filter",
        "ImageService",
        "IssueService",
        "Login",
        "MailService",
        "MineSaker",
        "NavbarMenu",
        "NotificationSettingsService",
        "OversiktOverSak",
        "RegisterAdmin",
        "SendTextMailWindow",
        "StatisticsService",
        "UserServices",
        "adminAddCategory",
        "adminIssues"
    ],
    "modules": [],
    "allModules": [],
    "elements": []
} };
});